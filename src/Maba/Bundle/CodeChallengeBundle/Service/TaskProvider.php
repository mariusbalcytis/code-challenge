<?php

namespace Maba\Bundle\CodeChallengeBundle\Service;

use Maba\Bundle\CodeChallengeBundle\Entity\Task;
use RuntimeException;

class TaskProvider
{
    /**
     * @var TestCaseProviderInterface[][]
     */
    private $testCaseProviders = [];

    public function registerTestCaseProvider(TestCaseProviderInterface $testCaseProvider, $taskIdentifier)
    {
        $this->testCaseProviders[$taskIdentifier][] = $testCaseProvider;
    }

    /**
     * @param string $taskIdentifier
     * @param string $hostDir
     * @param string $containerDir
     * @param array $parameters
     * @return Task
     */
    public function generateTask($taskIdentifier, $hostDir, $containerDir, array $parameters = [])
    {
        if (!isset($this->testCaseProviders[$taskIdentifier])) {
            throw new RuntimeException(sprintf('No test case providers registered for task %s', $taskIdentifier));
        }

        $task = (new Task())
            ->setTaskIdentifier($taskIdentifier)
        ;

        foreach ($this->testCaseProviders[$taskIdentifier] as $i => $testCaseProvider) {
            $task->addTestCase($testCaseProvider->generateTestCase($hostDir, $containerDir, $parameters, (string)$i));
        }

        return $task;
    }
}
