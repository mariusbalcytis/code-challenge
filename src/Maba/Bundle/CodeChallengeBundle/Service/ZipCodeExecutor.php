<?php

namespace Maba\Bundle\CodeChallengeBundle\Service;

use Comodojo\Zip\Zip;
use Maba\Component\RandomGenerator\RandomGeneratorInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Filesystem\Filesystem;

class ZipCodeExecutor
{
    private $cacheDir;
    private $filesystem;
    private $logger;
    private $randomGenerator;
    private $taskExecutor;

    public function __construct(
        $cacheDir,
        Filesystem $filesystem,
        LoggerInterface $logger,
        RandomGeneratorInterface $randomGenerator,
        TaskExecutor $taskExecutor
    ) {
        $this->cacheDir = $cacheDir;
        $this->filesystem = $filesystem;
        $this->logger = $logger;
        $this->randomGenerator = $randomGenerator;
        $this->taskExecutor = $taskExecutor;
    }

    public function execute($zipPath, $taskIdentifier)
    {
        $baseDir = $this->cacheDir . '/' . $this->randomGenerator->generate();
        $dir = $baseDir . '/code';
        $inputDir = $baseDir . '/input';

        $this->filesystem->mkdir($dir);
        $this->filesystem->mkdir($inputDir);

        try {
            $zip = Zip::open($zipPath);
            $zip->extract($dir);

            $parameters = [];   // taken by task generator, for example "script" (app.php etc)
            $type = 'php';

            return $this->taskExecutor->execute($taskIdentifier, $type, $dir, $inputDir, $parameters);
        } finally {
            $this->filesystem->remove($baseDir);
        }
    }
}
