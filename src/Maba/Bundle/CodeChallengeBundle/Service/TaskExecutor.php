<?php

namespace Maba\Bundle\CodeChallengeBundle\Service;

use Maba\Bundle\CodeChallengeBundle\Entity\TaskResult;
use Maba\Bundle\CodeChallengeBundle\Entity\TestCase;
use Maba\Bundle\CodeChallengeBundle\Entity\TestCaseResult;
use Maba\Bundle\CodeChallengeBundle\Event\TestCaseExecutionEvent;
use Maba\Bundle\CodeChallengeBundle\TestCaseEvents;
use Maba\Bundle\CodeExecutorBundle\Entity\CommandResult;
use Maba\Bundle\CodeExecutorBundle\Entity\ExecutionParameters;
use Maba\Bundle\CodeExecutorBundle\Entity\ExecutionResult;
use Maba\Bundle\CodeExecutorBundle\Service\CodeExecutorManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Filesystem\Filesystem;

class TaskExecutor
{
    private $taskProvider;
    private $logger;
    private $codeExecutorManager;
    private $filesystem;
    private $eventDispatcher;

    public function __construct(
        CodeExecutorManager $codeExecutorManager,
        TaskProvider $taskProvider,
        LoggerInterface $logger,
        Filesystem $filesystem,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->codeExecutorManager = $codeExecutorManager;
        $this->taskProvider = $taskProvider;
        $this->logger = $logger;
        $this->filesystem = $filesystem;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function execute($taskIdentifier, $type, $codeDir, $inputDir, array $parameters = [])
    {
        $containerInputDir = '/input';
        $task = $this->taskProvider->generateTask($taskIdentifier, $inputDir, $containerInputDir, $parameters);

        $executionParameters = (new ExecutionParameters())
            ->setType($type)
            ->setCodeDir($codeDir)
            ->setAdditionalDirectories([$containerInputDir => $inputDir])
        ;

        $testCases = [];
        foreach ($task->getTestCases() as $i => $testCase) {
            $testCaseKey = (string)$i;
            $testCases[$testCaseKey] = $testCase;
            $executionParameters->addCommand($testCaseKey, $testCase->getArguments());
        }

        $this->eventDispatcher->dispatch(
            TestCaseEvents::BEFORE_TEST_CASE_EXECUTED,
            new TestCaseExecutionEvent($task, $executionParameters)
        );

        $executionResult = $this->codeExecutorManager->execute($executionParameters);

        return $this->buildTaskResult($executionResult, $testCases);
    }

    /**
     * @param ExecutionResult $executionResult
     * @param TestCase[] $testCases
     * @return TaskResult
     */
    private function buildTaskResult(ExecutionResult $executionResult, array $testCases)
    {
        $taskResult = new TaskResult();
        $atLeastOnePassed = false;
        $hasFailures = false;
        $hasCriticalFailures = false;

        foreach ($executionResult->getCommandResults() as $testCaseKey => $commandResult) {
            $testCase = $testCases[$testCaseKey];
            $testCaseResult = $this->buildTestCaseResult($testCase, $commandResult);

            $taskResult->addTestCaseResult($testCaseResult);
            if ($testCaseResult->isPassed()) {
                $atLeastOnePassed = true;
            } else {
                $hasFailures = true;
                if ($testCase->getType() === TestCase::TYPE_CRITICAL) {
                    $hasCriticalFailures = true;
                }
            }
        }

        if ($atLeastOnePassed && !$hasFailures) {
            $taskResult->setStatus(TaskResult::STATUS_COMPLETED);
        } elseif ($atLeastOnePassed && !$hasCriticalFailures) {
            $taskResult->setStatus(TaskResult::STATUS_COMPLETED_WITH_FAILURES);
        }

        return $taskResult;
    }

    private function buildTestCaseResult(TestCase $testCase, CommandResult $commandResult)
    {
        $testCaseResult = (new TestCaseResult())
            ->setTestCase($testCase)
            ->setCommandResult($commandResult)
        ;

        if ($commandResult->getStatusCode() === $testCase->getExpectedStatusCode()) {
            if ($testCase->getExpectedOutput() === $commandResult->getOutput()) {
                $testCaseResult->setPassed(true);
            } else {
                $testCaseResult->setErrorCode(TestCaseResult::ERROR_CODE_WRONG_OUTPUT);
            }
        } elseif ($commandResult->getStatusCode() === 124) {
            $testCaseResult->setErrorCode(TestCaseResult::ERROR_CODE_TIMEOUT);
        } else {
            $testCaseResult->setErrorCode(TestCaseResult::ERROR_CODE_WRONG_STATUS_CODE);
        }

        return $testCaseResult;
    }
}
