<?php

namespace Maba\Bundle\CodeExecutorBundle\Entity;

class ExecutionParameters
{
    /**
     * @var string
     */
    private $type;

    /**
     * @var string directory where source code is. Will be mounted as /data and will be working directory
     */
    private $codeDir;

    /**
     * Commands to run.
     * Keys are identifiers, values are arrays of arguments without initial command - this depends on executor.
     *
     * @var array of array
     */
    private $commands = [];

    /**
     * @var array /path-in-container => /path-in-host
     */
    private $additionalDirectories = [];

    /**
     * @var array of array. Commands to run before main command. Should include initial command
     */
    private $prepareCommands = [];

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getCodeDir()
    {
        return $this->codeDir;
    }

    /**
     * @param string $codeDir
     * @return $this
     */
    public function setCodeDir($codeDir)
    {
        $this->codeDir = $codeDir;

        return $this;
    }

    /**
     * @return array
     */
    public function getCommands()
    {
        return $this->commands;
    }

    /**
     * @param string $commandId
     * @param array $command
     * @return $this
     */
    public function addCommand($commandId, array $command)
    {
        $this->commands[$commandId] = $command;

        return $this;
    }

    /**
     * @param array $commands
     * @return $this
     */
    public function setCommands(array $commands)
    {
        $this->commands = $commands;

        return $this;
    }

    /**
     * @return array
     */
    public function getAdditionalDirectories()
    {
        return $this->additionalDirectories;
    }

    /**
     * @param array $additionalDirectories
     * @return $this
     */
    public function setAdditionalDirectories(array $additionalDirectories)
    {
        $this->additionalDirectories = $additionalDirectories;

        return $this;
    }

    public function setAdditionalDirectory($containerPath, $hostPath)
    {
        $this->additionalDirectories[$containerPath] = $hostPath;

        return $this;
    }

    /**
     * @return array
     */
    public function getPrepareCommands()
    {
        return $this->prepareCommands;
    }

    /**
     * @param array $prepareCommands
     * @return $this
     */
    public function setPrepareCommands(array $prepareCommands)
    {
        $this->prepareCommands = $prepareCommands;

        return $this;
    }

    public function addPrepareCommand(array $prepareCommand)
    {
        $this->prepareCommands[] = $prepareCommand;

        return $this;
    }
}
