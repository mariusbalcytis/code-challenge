<?php

namespace Maba\Component\RandomGenerator;

class UrlSafeRandomGenerator implements RandomGeneratorInterface
{
    /**
     * @var int
     */
    private $randomBytes;

    public function __construct($randomBytes)
    {
        $this->randomBytes = $randomBytes;
    }

    public function generate()
    {
        return strtr(base64_encode(random_bytes(10)), ['/' => '_', '+' => '-', '=' => '']);
    }
}
