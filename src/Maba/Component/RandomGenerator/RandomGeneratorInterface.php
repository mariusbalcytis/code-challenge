<?php

namespace Maba\Component\RandomGenerator;

interface RandomGeneratorInterface
{

    public function generate();
}
