<?php

namespace Maba\Bundle\CodeExecutorBundle\Service;

use Maba\Bundle\CodeExecutorBundle\Entity\ExecutionParameters;
use Maba\Bundle\CodeExecutorBundle\Entity\ExecutionResult;

class CodeExecutorManager
{
    /**
     * @var CodeExecutor[]
     */
    private $codeExecutors = [];

    public function addCodeExecutor(CodeExecutor $codeExecutor, $type)
    {
        $this->codeExecutors[$type] = $codeExecutor;
    }

    /**
     * @param ExecutionParameters $executionParameters
     * @return ExecutionResult
     */
    public function execute(ExecutionParameters $executionParameters)
    {
        $type = $executionParameters->getType();

        if (!isset($this->codeExecutors[$type])) {
            throw new \RuntimeException(sprintf('No code executor with type "%s" registered', $type));
        }

        return $this->codeExecutors[$type]->execute($executionParameters);
    }
}
