<?php

namespace Maba\Bundle\CodeChallengeBundle\Entity;

class TestCase
{
    /**
     * Such test case must pass for task to be completed
     */
    const TYPE_CRITICAL = 'critical';

    /**
     * Such test case is not needed to pass for task completion. It's only for extra points
     */
    const TYPE_ADDITIONAL = 'additional';

    /**
     * @var string
     */
    private $type = self::TYPE_CRITICAL;

    /**
     * @var array
     */
    private $arguments;

    /**
     * @var string
     */
    private $expectedOutput;

    /**
     * @var int
     */
    private $expectedStatusCode = 0;

    /**
     * @var array
     */
    private $debugData = [];

    /**
     * @return array
     */
    public function getArguments()
    {
        return $this->arguments;
    }

    /**
     * @param array $arguments
     * @return $this
     */
    public function setArguments(array $arguments)
    {
        $this->arguments = $arguments;

        return $this;
    }

    /**
     * @return string
     */
    public function getExpectedOutput()
    {
        return $this->expectedOutput;
    }

    /**
     * @param string $expectedOutput
     * @return $this
     */
    public function setExpectedOutput($expectedOutput)
    {
        $this->expectedOutput = $expectedOutput;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return int
     */
    public function getExpectedStatusCode()
    {
        return $this->expectedStatusCode;
    }

    /**
     * @param int $expectedStatusCode
     * @return $this
     */
    public function setExpectedStatusCode($expectedStatusCode)
    {
        $this->expectedStatusCode = $expectedStatusCode;

        return $this;
    }

    /**
     * @return array
     */
    public function getDebugData()
    {
        return $this->debugData;
    }

    /**
     * @param array $debugData
     * @return $this
     */
    public function setDebugData(array $debugData)
    {
        $this->debugData = $debugData;

        return $this;
    }
}
