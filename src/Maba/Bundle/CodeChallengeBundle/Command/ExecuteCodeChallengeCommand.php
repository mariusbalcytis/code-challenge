<?php

namespace Maba\Bundle\CodeChallengeBundle\Command;

use Maba\Bundle\CodeChallengeBundle\Service\ZipCodeExecutor;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ExecuteCodeChallengeCommand extends ContainerAwareCommand
{
    private $zipCodeExecutor;

    public function __construct(ZipCodeExecutor $zipCodeExecutor)
    {
        parent::__construct();
        $this->zipCodeExecutor = $zipCodeExecutor;
    }

    protected function configure()
    {
        $this
            ->setName('maba:code-challenge:execute')
            ->setDescription('Runs given code with test cases and returns result')
            ->addArgument('zip', InputArgument::REQUIRED, 'Path to zipped code')
            ->addArgument('task', InputArgument::REQUIRED, 'Task identifier')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $zipPath = $input->getArgument('zip');
        $task = $input->getArgument('task');

        $result = $this->zipCodeExecutor->execute($zipPath, $task);

        $output->writeln(print_r($result, true));
    }
}
