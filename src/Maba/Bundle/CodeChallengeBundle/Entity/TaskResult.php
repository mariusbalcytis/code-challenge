<?php

namespace Maba\Bundle\CodeChallengeBundle\Entity;

class TaskResult
{
    const STATUS_FAILED = 'failed';
    const STATUS_COMPLETED_WITH_FAILURES = 'completed_with_failures';
    const STATUS_COMPLETED = 'completed';

    /**
     * @var string
     */
    private $status = self::STATUS_FAILED;

    /**
     * @var TestCaseResult[]
     */
    private $testCaseResults = [];

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return TestCaseResult[]
     */
    public function getTestCaseResults()
    {
        return $this->testCaseResults;
    }

    /**
     * @param TestCaseResult[] $testCaseResults
     * @return $this
     */
    public function setTestCaseResults(array $testCaseResults)
    {
        $this->testCaseResults = $testCaseResults;

        return $this;
    }

    /**
     * @param TestCaseResult $testCaseResult
     * @return $this
     */
    public function addTestCaseResult(TestCaseResult $testCaseResult)
    {
        $this->testCaseResults[] = $testCaseResult;

        return $this;
    }
}
