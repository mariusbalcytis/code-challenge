<?php

namespace Maba\Bundle\PhpCodeExecutorBundle\Listener;

use Maba\Bundle\CodeChallengeBundle\Event\TestCaseExecutionEvent;
use Maba\Bundle\PhpCodeExecutorBundle\CodeExecutorTypes;
use Maba\Bundle\PhpCodeExecutorBundle\Service\ComposerInstaller;
use Symfony\Component\Filesystem\Filesystem;

class InstallComposerListener
{
    private $composerInstaller;
    private $filesystem;

    public function __construct(ComposerInstaller $composerInstaller, Filesystem $filesystem)
    {
        $this->composerInstaller = $composerInstaller;
        $this->filesystem = $filesystem;
    }

    public function beforeTestCaseExecuted(TestCaseExecutionEvent $event)
    {
        if ($event->getExecutionParameters()->getType() !== CodeExecutorTypes::TYPE_PHP) {
            return;
        }

        $executionParameters = $event->getExecutionParameters();
        $codeDir = $executionParameters->getCodeDir();
        if (
            $this->filesystem->exists($codeDir . '/composer.json')
            && !$this->filesystem->exists($codeDir . '/vendor')
        ) {
            $this->composerInstaller->ensureComposerInstalled();
            $executionParameters->addPrepareCommand(['php', '/composer/composer.phar', 'install', '-o', '--no-dev']);
            $executionParameters->setAdditionalDirectory('/composer', $this->composerInstaller->getComposerDirectory());
        }
    }
}
