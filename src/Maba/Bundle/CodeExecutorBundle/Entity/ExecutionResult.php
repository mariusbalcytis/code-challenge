<?php

namespace Maba\Bundle\CodeExecutorBundle\Entity;

class ExecutionResult
{
    /**
     * Results of commands ran. Keys are command identifiers
     *
     * @var CommandResult[]
     */
    private $commandResults = [];

    /**
     * @return CommandResult[]
     */
    public function getCommandResults()
    {
        return $this->commandResults;
    }

    /**
     * @param CommandResult[] $commandResults
     */
    public function setCommandResults(array $commandResults)
    {
        $this->commandResults = $commandResults;
    }

    /**
     * @param string $commandId
     * @param CommandResult $commandResult
     * @return $this
     */
    public function addCommandResult($commandId, CommandResult $commandResult)
    {
        $this->commandResults[$commandId] = $commandResult;

        return $this;
    }
}
