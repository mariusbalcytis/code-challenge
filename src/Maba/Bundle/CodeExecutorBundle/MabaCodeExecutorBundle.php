<?php

namespace Maba\Bundle\CodeExecutorBundle;

use Maba\Component\DependencyInjection\AddTaggedCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class MabaCodeExecutorBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
        $container->addCompilerPass(new AddTaggedCompilerPass(
            'maba_code_executor.code_executor_manager',
            'maba_code_executor.code_executor',
            'addCodeExecutor',
            ['type']
        ));
    }

}
