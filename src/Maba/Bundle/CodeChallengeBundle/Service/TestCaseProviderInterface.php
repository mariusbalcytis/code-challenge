<?php

namespace Maba\Bundle\CodeChallengeBundle\Service;

use Maba\Bundle\CodeChallengeBundle\Entity\TestCase;

interface TestCaseProviderInterface
{

    /**
     * @param string $hostDir
     * @param string $containerDir
     * @param array $parameters
     * @param string $testCaseKey
     * @return TestCase
     */
    public function generateTestCase($hostDir, $containerDir, array $parameters, $testCaseKey);
}
