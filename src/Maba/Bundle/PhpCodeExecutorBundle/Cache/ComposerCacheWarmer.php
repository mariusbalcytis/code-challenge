<?php

namespace Maba\Bundle\PhpCodeExecutorBundle\Cache;

use Maba\Bundle\PhpCodeExecutorBundle\Service\ComposerInstaller;
use Symfony\Component\HttpKernel\CacheWarmer\CacheWarmerInterface;

class ComposerCacheWarmer implements CacheWarmerInterface
{
    private $composerInstaller;

    public function __construct(ComposerInstaller $composerInstaller)
    {
        $this->composerInstaller = $composerInstaller;
    }

    public function isOptional()
    {
        return true;
    }

    public function warmUp($cacheDir)
    {
        $this->composerInstaller->ensureComposerInstalled();
    }
}
