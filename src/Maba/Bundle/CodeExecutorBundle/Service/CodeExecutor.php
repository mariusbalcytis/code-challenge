<?php

namespace Maba\Bundle\CodeExecutorBundle\Service;

use Maba\Bundle\CodeExecutorBundle\Entity\CommandResult;
use Maba\Bundle\CodeExecutorBundle\Entity\ExecutionParameters;
use Maba\Bundle\CodeExecutorBundle\Entity\ExecutionResult;
use Maba\Component\RandomGenerator\RandomGeneratorInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Process\ProcessBuilder;

class CodeExecutor
{
    private $imageName;
    private $timeout;
    private $prepareTimeout;
    private $command;
    private $dockerBin;
    private $logger;
    private $randomGenerator;

    public function __construct(
        $dockerBin,
        $imageName,
        $timeout,
        $prepareTimeout,
        $command,
        LoggerInterface $logger,
        RandomGeneratorInterface $randomGenerator
    ) {
        $this->dockerBin = $dockerBin;
        $this->imageName = $imageName;
        $this->timeout = $timeout;
        $this->prepareTimeout = $prepareTimeout;
        $this->command = $command;
        $this->logger = $logger;
        $this->randomGenerator = $randomGenerator;
    }

    /**
     * @param ExecutionParameters $executionParameters
     * @return ExecutionResult
     */
    public function execute(ExecutionParameters $executionParameters)
    {
        $this->logger->info('Executing code', [$executionParameters]);

        $containerName = 'tmp-code-executor-' . $this->randomGenerator->generate();

        $this->createContainer(
            $containerName,
            ['/data' => $executionParameters->getCodeDir()] + $executionParameters->getAdditionalDirectories()
        );

        try {
            foreach ($executionParameters->getPrepareCommands() as $prepareCommand) {
                $this->runPrepareCommand($containerName, $prepareCommand);
            }

            $executionResult = new ExecutionResult();
            foreach ($executionParameters->getCommands() as $commandId => $commandArguments) {
                $executionResult->addCommandResult($commandId, $this->runCommand($containerName, $commandArguments));
            }

        } finally {
            $this->removeContainer($containerName);
        }

        return $executionResult;
    }

    private function createContainer($containerName, $directories)
    {
        $command = array_merge(
            [
                $this->dockerBin,
                'run',
                '--name',
                $containerName,
                '-d',
            ],
            array_reduce(
                array_keys($directories),
                function ($params, $dir) use ($directories) {
                    return array_merge($params, ['-v', $directories[$dir].':'.$dir]);
                },
                []
            ),
            [
                '-w',
                '/data',
                $this->imageName,
            ],
            [
                'tail',
                '-f',
                '/dev/null',
            ]
        );

        $builder = new ProcessBuilder($command);
        $process = $builder->getProcess();
        $this->logger->debug('Creating container', [$process->getCommandLine()]);
        $process->mustRun();
    }

    private function runPrepareCommand($containerName, $prepareCommand)
    {
        $command = array_merge(
            [
                $this->dockerBin,
                'exec',
                '-u',
                '1000:1000',
                $containerName,
                'timeout',
                $this->prepareTimeout,
            ],
            $prepareCommand
        );

        $builder = new ProcessBuilder($command);
        $process = $builder->getProcess();
        $this->logger->debug('Running prepare process', [$process->getCommandLine()]);
        $process->mustRun();
    }

    private function runCommand($containerName, $arguments)
    {
//        array_shift($arguments); // todo: needed if no need for "php"
        $command = array_merge(
            [
                $this->dockerBin,
                'exec',
                '-u',
                getmyuid() . ':' . getmygid(),
                $containerName,
                'timeout',
                $this->timeout,
                $this->command,
            ],
        // todo: pass something else than "script.php" or uncomment temporarily
//        ['bin/console', 'operation:calculate-fee'],
//        ['bin/console', 'calculate-commissions'],
            $arguments
        );

        $builder = new ProcessBuilder($command);
        $process = $builder->getProcess();

        $this->logger->debug('Running process', [$process->getCommandLine()]);

        $startTime = microtime(true);
        $statusCode = $process->run();
        $executionTime = microtime(true) - $startTime;

        $result = new CommandResult();
        $result
            ->setStatusCode($statusCode)
            ->setOutput($process->getOutput())
            ->setErrorOutput($process->getErrorOutput())
            ->setExecutionTime($executionTime);

        $this->logger->debug('Got result', [$result]);

        return $result;
    }

    private function removeContainer($containerName)
    {
        $builder = new ProcessBuilder([$this->dockerBin, 'rm', '-f', $containerName]);
        $process = $builder->getProcess();
        $this->logger->debug('Removing container', [$process->getCommandLine()]);
        $process->mustRun();
    }
}
