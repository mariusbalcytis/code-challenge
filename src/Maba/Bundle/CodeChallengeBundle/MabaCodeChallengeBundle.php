<?php

namespace Maba\Bundle\CodeChallengeBundle;

use Maba\Component\DependencyInjection\AddTaggedByPriorityCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class MabaCodeChallengeBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new AddTaggedByPriorityCompilerPass(
            'maba_code_challenge.task_provider',
            'maba_code_challenge.test_case_provider',
            'registerTestCaseProvider',
            ['task_identifier']
        ));
    }
}
