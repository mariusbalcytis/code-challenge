<?php

namespace Maba\Bundle\PhpCodeExecutorBundle\Service;

use Symfony\Component\Filesystem\Filesystem;

class ComposerInstaller
{

    private $composerDirectory;
    private $filesystem;

    public function __construct($composerDirectory, Filesystem $filesystem)
    {
        $this->composerDirectory = $composerDirectory;
        $this->filesystem = $filesystem;
    }

    public function updateComposer()
    {
        $this->filesystem->mkdir($this->composerDirectory);
        $contents = file_get_contents('https://getcomposer.org/composer.phar');
        if (!$contents) {
            return false;
        }

        file_put_contents($this->composerDirectory . '/composer.phar', $contents);
        return true;
    }

    public function ensureComposerInstalled()
    {
        if ($this->filesystem->exists($this->composerDirectory . '/composer.phar')) {
            return;
        }

        if (!$this->updateComposer()) {
            throw new \RuntimeException(
                sprintf('Composer not available in %s and download failed', $this->composerDirectory . '/composer.phar')
            );
        }
    }

    public function getComposerDirectory()
    {
        return $this->composerDirectory;
    }
}
