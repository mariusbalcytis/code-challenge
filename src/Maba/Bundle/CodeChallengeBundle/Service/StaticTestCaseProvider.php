<?php

namespace Maba\Bundle\CodeChallengeBundle\Service;

use Maba\Bundle\CodeChallengeBundle\Entity\TestCase;
use Symfony\Component\Config\FileLocatorInterface;

class StaticTestCaseProvider implements TestCaseProviderInterface
{
    private $fileLocator;
    private $defaultScriptName;
    private $inputFilenamePattern;
    private $realInputFilename;
    private $expectedOutputFilename;
    private $critical = true;

    /**
     * @param FileLocatorInterface $fileLocator
     * @param string $defaultScriptName For example script.php. Can be overriden with $parameters['script']
     * @param string $inputFilenamePattern Place # where test case key should be placed, for example input#.csv
     * @param string $realInputFilename Path in system where input contents for this test case is placed
     * @param string $expectedOutputFilename Path in system where expected output for this test case is placed
     */
    public function __construct(
        FileLocatorInterface $fileLocator,
        $defaultScriptName,
        $inputFilenamePattern,
        $realInputFilename,
        $expectedOutputFilename
    ) {
        $this->fileLocator = $fileLocator;
        $this->defaultScriptName = $defaultScriptName;
        $this->inputFilenamePattern = $inputFilenamePattern;
        $this->realInputFilename = $realInputFilename;
        $this->expectedOutputFilename = $expectedOutputFilename;
    }

    public function markAsAdditional()
    {
        $this->critical = false;
    }


    public function generateTestCase($hostDir, $containerDir, array $parameters, $testCaseKey)
    {
        $inputDefaultFilename = str_replace('#', '', $this->inputFilenamePattern);
        $inputFilename = str_replace('#', $testCaseKey, $this->inputFilenamePattern);
        $inputData = file_get_contents($this->fileLocator->locate($this->realInputFilename));
        $expectedOutput = file_get_contents($this->fileLocator->locate($this->expectedOutputFilename));

        $scriptName = isset($parameters['script']) ? $parameters['script'] : $this->defaultScriptName;

        file_put_contents($hostDir . '/' . $inputFilename, $inputData);

        $testCase = (new TestCase())
            ->setArguments([$scriptName, $containerDir . '/' . $inputFilename])
            ->setExpectedOutput($expectedOutput)
            ->setDebugData([$inputDefaultFilename => $inputData])
        ;
        if (!$this->critical) {
            $testCase->setType(TestCase::TYPE_ADDITIONAL);
        }
        return $testCase;
    }
}
