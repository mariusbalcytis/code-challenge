<?php

namespace Maba\Bundle\CodeChallengeBundle\Entity;

use Maba\Bundle\CodeExecutorBundle\Entity\CommandResult;

class TestCaseResult
{
    const ERROR_CODE_TIMEOUT = 'timeout';
    const ERROR_CODE_WRONG_STATUS_CODE = 'wrong_status_code';
    const ERROR_CODE_WRONG_OUTPUT = 'wrong_output';

    /**
     * @var boolean
     */
    private $passed = false;

    /**
     * @var string
     */
    private $errorCode;

    /**
     * @var CommandResult
     */
    private $commandResult;

    /**
     * @var TestCase
     */
    private $testCase;

    /**
     * @return boolean
     */
    public function isPassed()
    {
        return $this->passed;
    }

    /**
     * @param boolean $passed
     * @return $this
     */
    public function setPassed($passed)
    {
        $this->passed = $passed;

        return $this;
    }

    /**
     * @return string
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @param string $errorCode
     * @return $this
     */
    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;

        return $this;
    }

    /**
     * @return CommandResult
     */
    public function getCommandResult()
    {
        return $this->commandResult;
    }

    /**
     * @param CommandResult $commandResult
     * @return $this
     */
    public function setCommandResult(CommandResult $commandResult)
    {
        $this->commandResult = $commandResult;

        return $this;
    }

    /**
     * @return TestCase
     */
    public function getTestCase()
    {
        return $this->testCase;
    }

    /**
     * @param TestCase $testCase
     * @return $this
     */
    public function setTestCase(TestCase $testCase)
    {
        $this->testCase = $testCase;

        return $this;
    }
}
