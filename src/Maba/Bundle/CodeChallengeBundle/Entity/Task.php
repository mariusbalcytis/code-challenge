<?php

namespace Maba\Bundle\CodeChallengeBundle\Entity;

class Task
{
    /**
     * @var string
     */
    private $taskIdentifier;

    /**
     * @var TestCase[]
     */
    private $testCases = [];

    /**
     * @return string
     */
    public function getTaskIdentifier()
    {
        return $this->taskIdentifier;
    }

    /**
     * @param string $taskIdentifier
     * @return $this
     */
    public function setTaskIdentifier($taskIdentifier)
    {
        $this->taskIdentifier = $taskIdentifier;

        return $this;
    }

    /**
     * @return TestCase[]
     */
    public function getTestCases()
    {
        return $this->testCases;
    }

    /**
     * @param TestCase[] $testCases
     * @return $this
     */
    public function setTestCases(array $testCases)
    {
        $this->testCases = $testCases;

        return $this;
    }

    /**
     * @param TestCase $testCase
     * @return $this
     */
    public function addTestCase(TestCase $testCase)
    {
        $this->testCases[] = $testCase;

        return $this;
    }
}
