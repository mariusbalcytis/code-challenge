<?php

namespace Maba\Bundle\CodeChallengeBundle\Event;

use Maba\Bundle\CodeChallengeBundle\Entity\Task;
use Maba\Bundle\CodeExecutorBundle\Entity\ExecutionParameters;
use Symfony\Component\EventDispatcher\Event;

class TestCaseExecutionEvent extends Event
{
    /**
     * @var Task
     */
    private $task;

    /**
     * @var ExecutionParameters
     */
    private $executionParameters;

    public function __construct(Task $task, ExecutionParameters $executionParameters)
    {
        $this->task = $task;
        $this->executionParameters = $executionParameters;
    }

    /**
     * @return Task
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * @return ExecutionParameters
     */
    public function getExecutionParameters()
    {
        return $this->executionParameters;
    }
}
